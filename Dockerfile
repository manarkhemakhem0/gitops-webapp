FROM scratch

LABEL maintaner="Manar khemakhem <manarkhemakhem0@gmail.com>"

COPY . .

EXPOSE 8080

CMD ["./main"]
